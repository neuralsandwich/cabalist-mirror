import requests
import json
import sys


class MergeRequests:

    """
    token  - Token to be stored in header
    remote - Object storing information about remote repository
    """
    def __init__(self, remote=None, args=None):
        self.remote = remote
        self.args = args

    def usage(self):
        print "Usage cab merge:\n\n* merge\n* create\n* list\n* close"

    def parse(self):
        # Merge List
        if self.args['merge'] == 'list':
            # High jacking list to get single merge request
            try:
                self.get_merge_request(merge_id=self.args['-i'])
            except KeyError as e:
                if '-i' in e.args:
                    self.list()
                else:
                    print(e)

        # Merge create
        elif self.args['merge'] == 'create':
            try:
                self.create(title=self.args['--title'],
                            source=self.args['--source'],
                            target=self.args['--target'])
            except KeyError as e:
                if '--target' in e.args:
                    self.args['--target'] = 'master'
                    self.create(title=self.args['--title'],
                                source=self.args['--source'],
                                target=self.args['--target'])
                else:
                    print("Missing arguments: {0}").format(e.args)

        # Merge Close
        elif self.args['merge'] == 'close':
            try:
                self.close_merge_request(merge_id=self.args['-i'])
            except KeyError as e:
                print("Missing arguments: {0}").format(e.args)

        # Merge accept
        elif self.args['merge'] == 'accept':
            try:
                self.accept_merge_request(merge_id=self.args['-i'])
            except KeyError as e:
                print("Missing arguments: {0}").format(e.args)

        # Command error
        else:
            print(self.args['merge'] + " is not a valid merge command")
            sys.exit(1)

    def create(self, title=None, source=None, target='master'):
        url, header = self.remote.get_merge_requests_url()
        data = self.remote.create_merge_data(title=title, source_branch=source,
                                             target_branch=target)
        try:
            r = requests.post(url, data=json.dumps(data), headers=header)

            if r.status_code is not 201:
                print(r.json()['message'][0])

        except Exception as e:
            print ("Error: Failed to create Merge request")
            print(e)

    def list(self, state="opened", order_by=None, sort=None):
        url, header = self.remote.get_merge_requests_url()
        data = self.remote.get_merge_list(state, order_by, sort)
        try:
            r = requests.get(url, params=data, headers=header)
            # TODO Add some sort of formatted output
            for mr in r.json():
                print(str(mr['id']) + '\t' + mr['title'] + '\t'
                      + mr['state'] + '\t' + mr['author']['name'] + '\t'
                      + mr['source_branch'] + ' > ' + mr['target_branch'])

        except Exception as e:
            print ("Error: Failed to get merge requests")
            print(e)

    def get_merge_request(self, merge_id=None):
        url, header = self.remote.get_merge_request_url()

        try:
            r = requests.get(url + merge_id, headers=header)
            # TODO Add some sort of formatted output
            if r.status_code != 404:
                mr = r.json()
                if (mr['description'] is not None and
                   mr['assignee']['name'] is not None):
                    print('#' + str(mr['iid']) + ' ' + mr['title']
                          + '\tstatus: '
                          + mr['state'] + '\nAuthor: ' + mr['author']['name']
                          + '\t' + mr['source_branch'] + ' > '
                          + mr['target_branch'] + '\nAssingee: '
                          + mr['assignee']['name'] + '\n\n'
                          + mr['description'])
                else:
                    print(str(mr['id']) + '\t' + mr['title'] + '\t'
                          + mr['state'] + '\t' + mr['author']['name'] + '\t'
                          + mr['source_branch'] + ' > ' + mr['target_branch'])
            else:
                print('Error: Merge request does not exist')
                raise Exception

        except Exception as e:
            print ("Error: Failed to get merge request")
            print(e)

    def close_merge_request(self, merge_id=None):
        url, header = self.remote.get_merge_request_url()
        url += merge_id
        data = self.remote.get_merge_update(state_event="close")

        try:
            r = requests.put(url, data=json.dumps(data), headers=header)
            self.get_merge_request(merge_id)
        except Exception as e:
            print
            print(e)

    def accept_merge_request(self, merge_id=None,
                             merge_commit_message=None):
        url, header = self.remote.get_merge_request_url()
        url += merge_id + '/merge'
        data = self.remote.create_merge_accept(merge_commit_message)

        try:
            r = requests.put(url, data=json.dumps(data), headers=header)
            self.get_merge_request(merge_id)
        except Exception as e:
            print
            print(e)

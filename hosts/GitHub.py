class GitHub:

    def __init__(self, base_url="https://api.github.com"):
        if base_url is not "https://api.github.com":
            self.base_url = base_url

    def get_merge_url(self, namespace, project):
        return self.base_url + "/repos/" + namespace + "/" + project + "pulls"

    def self_hosted(self):
        if self.base_url is "https://api.github.com":
            return False

        return True

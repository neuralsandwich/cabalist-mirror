import traceback
import sys
import os
import json
import string


class ProjectConfig:
    """Represents the project configuration"""

    def __init__(self):
        try:
            file = open(os.getcwd() + '/.cabconfig')
            json_data = json.load(file)
            file.close()
            self.project = json_data['project']
            self.uri = json_data['uri']
            self.host = json_data['host']

            self.namespace = string.split(json_data['uri'], '/')
            self.namespace = self.namespace[len(self.namespace)-2]

            self.base_url = string.replace(json_data['uri'],
                                           self.namespace + '/'
                                           + self.project, "")

        except:
            print "Fatal: Could not find project config. No .cabconfig found"
            sys.exit(2)


class UserConfig:
    """Represents the users configuration"""

    def __init__(self, base_url):
        try:
            file = open(os.path.expanduser('~/.cabalistrc'))
            json_data = json.load(file)
            file.close()
            self.token = json_data[string.split(base_url, '/')[2]]
        except KeyError as e:
            print(e)
        except Exception as e:
            print "Fatal: No user config. Could not find ~/.cabalistrc"
            print(e)

import sys
import requests
import string


class GitLab:

    def __init__(self, project=None, token=None):
        self.project = project
        self.token = token
        self.base_url = self.get_api_url(project.uri)
        self.header = {'content-type': 'application/json',
                       'private-token': self.token}

    def get_api_url(self, uri=None):
        if uri is None:
            return "https://gitlab.com/api/v3"

        if 'https://gitlab.com/' in uri:
            return "https://gitlab.com/api/v3"

        url = string.replace(uri, self.project.namespace + '/'
                             + self.project.project, "")
        url += "api/v3"
        return url

    def get_merge_requests_url(self):
        url = (self.base_url + "/projects/" + str(self.get_project_id())
               + "/merge_requests")
        return url, self.header

    def get_merge_request_url(self):
        url = (self.base_url + "/projects/" + str(self.get_project_id())
               + "/merge_request/")
        return url, self.header

    def self_hosted(self):
        if self.base_url is "https://gitlab.com/api/v3":
            return False

        return True

    def get_project_id(self):
        url = (self.base_url + "/projects/" + self.project.namespace + "%2F"
               + self.project.project)

        header = {'content-type': 'application/json',
                  'private-token': self.token}
        try:
            r = requests.get(url, headers=header)
            return r.json()['id']

        except Exception as e:
            print("Fatal: Could not retrieve project id")
            print(e)
            sys.exit(1)

    def get_merge_list(self, state=None, order_by=None, sort=None):
        data = {}

        try:
            if state is ("opened" or "closed" or "merged" or "all"):
                print("Open Merge Requests")
                data['state'] = state
            if order_by is ("created_at" or "updated_at"):
                data['order_by'] = order_by
            if sort is ("asc" or "desc"):
                data['sort'] = sort

        except Exception as e:
            print(e)

        return data

    def create_merge_data(self, title, source_branch=None, target_branch=None):
        data = {'source_branch': source_branch, 'target_branch': target_branch,
                'title': title}
        return data

    def get_merge_update(self, merge_request_id=None, source_branch=None,
                         target_branch=None, assignee_id=None, title=None,
                         state_event=None):
        data = {}

        try:
            if merge_request_id is not None:
                data['merge_request_id'] = merge_request_id
            if source_branch is not None:
                data['source_branch'] = source_branch
            if target_branch is not None:
                data['target_branch'] = target_branch
            if assignee_id is not None:
                data['assignee_id'] = assignee_id
            if title is not None:
                data['title'] = title
            if state_event is not None:
                data['state_event'] = state_event

        except Exception as e:
            print(e)

        return data

    def create_merge_accept(self, merge_commit_message=None):
        data = {}

        try:
            if merge_commit_message is not None:
                data['merge_commit_message'] = merge_commit_message
        except Exception as e:
            print(e)

        return data
